import axios from 'axios';

import { GET_REVIEWS, ADD_REVIEW } from './types.js';

export const getReviews = (id) => dispatch => {
	axios.get(`/api/reviews/?movie=${id}`)
		.then(res => {
			dispatch({
				type: GET_REVIEWS,
				payload: res.data
			});
		}).catch(err => console.log(err))
}

export const addReview = review => dispatch => {
	axios.post(`/api/reviews/`, review)
		.then(res => {
			dispatch({
				type: ADD_REVIEW,
				payload: res.data
			});
		}).catch(err => console.log(err))
}