export const GET_MOVIES = 'GET_MOVIES';
export const DELETE_MOVIE = 'DELETE_MOVIE';
export const ADD_MOVIE = 'ADD_MOVIE';
export const GET_MOVIE = 'GET_MOVIE';
export const GET_REVIEWS = 'GET_REVIEWS';
export const ADD_REVIEW = 'ADD_REVIEW';
