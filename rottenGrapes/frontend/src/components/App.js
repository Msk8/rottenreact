import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';

import Header from './layout/Header.js';
import Dashboard from './movies/Dashboard.js';

import Section from './reviews/Section.js'

import { Provider } from 'react-redux';
import store from "../store.js";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class App extends Component{
	render() {
		return (
			<Provider store={store}>
				<Fragment>
					<Header />
					<div className="container">
						<Dashboard />
					</div>
				</Fragment>
			</Provider>
		) 
	}
}

class Review extends Component{
	constructor(props){
		super(props)
	}
	render(){
		return(
			<Provider store={store}>
		        <Fragment>
		          <Header />
		          <div className="container">
		            <Section id={this.props.match.params.id} />
		   
		          </div>
		        </Fragment>
		    </Provider>
		)
	}
}


class Menu extends Component {
	render(){
		return (
			<Router>
	          <div>
	            <Route path="/" exact component={App} />
	            <Route path="/review/:id" component={Review} />
	          </div>
	        </Router>
		)
	}
}


ReactDOM.render(<Menu />, document.getElementById('app'))
