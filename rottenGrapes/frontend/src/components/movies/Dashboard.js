import React, {Fragment} from 'react';
import Form from './Form.js';
import Movies from './Movies.js';

export default function Dashboard(props) {
	return (
		<Fragment>
			<Form />
			<Movies />
		</Fragment>
	)
}