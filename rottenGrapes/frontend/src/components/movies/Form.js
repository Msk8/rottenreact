import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { addMovie} from '../../actions/movies.js'

export class Form extends Component {
	state = {
		name: '',
		release_date:'',
		classification:'',
		genre:'',
		duration:''
	}

	static propTypes = {
		addMovie: PropTypes.func.isRequired
	}


	onChange = e => this.setState({ [e.target.name]:e.target.value });

	onSubmit = e => {
		e.preventDefault();
		const { name, release_date, classification, genre, duration } = this.state;
		const movie = { name, release_date, classification, genre, duration };
		console.log(movie)
		console.log(this.state)
		this.props.addMovie(movie);
		console.log("submit");
	};


	render() {
		const { name, release_date, classification, genre, duration } = this.state;

		return (
			<div className="card card-body mt-4 mb-4">
		        <h2>Add Movie</h2>
		        <form onSubmit={this.onSubmit}>
		          <div className="form-group">
		            <label>Name</label>
		            <input
		              className="form-control"
		              type="text"
		              name="name"
		              onChange={this.onChange}
		              value={name}
		            />
		          </div>
		          <div className="form-group">
		            <label>Release date</label>
		            <input
		              className="form-control"
		              type="date"
		              name="release_date"
		              onChange={this.onChange}
		              value={release_date}
		            />
		          </div>
		          <div className="form-group">
		            <label>Classification</label>
		            <textarea
		              className="form-control"
		              type="text"
		              name="classification"
		              onChange={this.onChange}
		              value={classification}
		            />
		          </div>
		          <div className="form-group">
		            <label>Genre</label>
		            <textarea
		              className="form-control"
		              type="text"
		              name="genre"
		              onChange={this.onChange}
		              value={genre}
		            />
		          </div>
				  <div className="form-group">
		            <label>Duration</label>
		            <textarea
		              className="form-control"
		              type="text"
		              name="duration"
		              onChange={this.onChange}
		              value={duration}
		            />
		          </div>	

		          <div className="form-group">
		            <button type="submit" className="btn btn-primary">
		              Submit
		            </button>
		          </div>
		        </form>
		      </div>
		)
	}
}

export default connect(null, {addMovie})(Form);