import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getMovies, deleteMovie } from '../../actions/movies.js'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Section from '../reviews/Section.js'


export class Movies extends Component {
	static propTypes = {
		movies: PropTypes.array.isRequired,
		getMovies: PropTypes.array.isRequired,
		deleteMovie: PropTypes.array.isRequired
	};

	componentDidMount(){
		this.props.getMovies();
	}

	render() {
		return (

			<Fragment>
				<h2> Movies</h2>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Release Date</th>
							<th>Classification</th>
							<th>Genre</th>
							<th>Duration</th>
						</tr>
					</thead>
					<tbody>
						{ this.props.movies.map(movie =>
							<tr key={movie.id}>
								<td>{movie.id}</td>
								<td>{movie.name}</td>
								<td>{movie.release_date}</td>
								<td>{movie.classification}</td>
								<td>{movie.genre}</td>
								<td>{movie.duration}</td>
								<td>
									<button onClick={this.props.deleteMovie.bind(this, movie.id)} className="btn btn-danger btn-sm">{" "}Delete</button>
								</td>
								<td>
									<Link to={`/review/${movie.id}`}>Details & Reviews</Link>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	movies: state.movies.movies
});

export default connect(mapStateToProps, {getMovies, deleteMovie})(Movies);