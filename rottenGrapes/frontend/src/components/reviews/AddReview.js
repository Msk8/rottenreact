import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { addReview } from '../../actions/reviews.js'

export class AddReview extends Component {
	state = {
		comment: '',
		star_rating:''
	}

	static propTypes = {
		addReview: PropTypes.func.isRequired
	}


	onChange = e => this.setState({ [e.target.name]:e.target.value });

	onSubmit = e => {
		e.preventDefault();
		const { comment, star_rating } = this.state;
		const review = { comment, star_rating };
		this.props.addReview(review);
		console.log("submit review");
	};


	render() {
		

		const { comment, star_rating } = this.state;

		return (
			<div className="card card-body mt-4 mb-4">
		        <h2>Add Review</h2>
		        <form onSubmit={this.onSubmit}>
		          <div className="form-group">
		            <label>Comment</label>
		            <input
		              className="form-control"
		              type="textarea"
		              name="comment"
		              onChange={this.onChange}
		              value={comment}
		            />
		          </div>
		          <div className="form-group">
		            <label>Star Rating</label>
		            <input
		              className="form-control"
		              type="number"
		              name="star_rating"
		              onChange={this.onChange}
		              value={star_rating}
		            />
		          </div>
		          

		          <div className="form-group">
		            <button type="submit" className="btn btn-primary">
		              Submit
		            </button>
		          </div>
		        </form>
		      </div>
		)
	}
}

export default connect(null, {addReview})(AddReview);