import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getMovie } from '../../actions/movies.js'

export class Details extends Component {
	static propTypes = {
		movie: PropTypes.element.isRequired
	};

	componentDidMount(){
		this.props.getMovie(this.props.id);
	}


	render() {
		const {movie} = this.props;

		return (
			<Fragment>
				 <h1 class="my-4">{movie.name} <br />
				    <small>Year: {movie.year_of_release}</small>
				  </h1>

				  
				  <div class="row">

				    <div class="col-md-4">
				      <h3 class="my-3">Details</h3>
				      				   
				      <ul>
				        <li>Release Date: {movie.release_date}</li>
				        <li>Classification: {movie.classification}</li>
				        <li>Genre: {movie.genre}</li>
				        <li>Duration: {movie.duration}</li>
				      </ul>
				    </div>

				  </div>

			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	movie: state.movies.movie
});

export default connect(mapStateToProps, {getMovie})(Details);