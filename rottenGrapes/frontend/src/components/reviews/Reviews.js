import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getReviews } from '../../actions/reviews.js'




export class Reviews extends Component {
	static propTypes = {
		reviews: PropTypes.array.isRequired,
		getReviews: PropTypes.array.isRequired
	};

	componentDidMount(){
		this.props.getReviews(this.props.id);
	}

	render() {
		return (
			<Fragment>
				<h2> Reviews</h2>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Comment</th>
							<th>Star Rating</th>
						</tr>
					</thead>
					<tbody>
						{ this.props.reviews.map(review =>
							<tr key={review.id}>
								<td>{review.id}</td>
								<td>{review.comment}</td>
								<td>{review.star_rating}</td>
							</tr>
						)}
					</tbody>
				</table>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	reviews: state.reviews.reviews
});

export default connect(mapStateToProps, {getReviews})(Reviews);