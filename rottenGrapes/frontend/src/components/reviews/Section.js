import React, {Fragment} from 'react';
import Details from './Details.js';
import AddReview from './AddReview.js';
import Reviews from './Reviews.js';


export default function Section(props) {
	return (
		<Fragment>
			<Details id={props.id}/>
			<AddReview id={props.id}/>
			<Reviews id={props.id}/>
		</Fragment>
	)
}