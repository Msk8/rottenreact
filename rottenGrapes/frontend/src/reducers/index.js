import { combineReducers } from 'redux';
import movies from './movies.js'
import reviews from './reviews.js'

export default combineReducers({
	movies, reviews
});