import { GET_MOVIES, DELETE_MOVIE, ADD_MOVIE, GET_MOVIE } from '../actions/types.js'

const initialState = {
	something: 'text',
	movies: [],
	movie: {}
}

export default function(state = initialState, action) {
	switch(action.type){
		case GET_MOVIES:
			return{
				...state,
				movies: action.payload
			}

		case DELETE_MOVIE:
			return{
				...state,
				movies: state.movies.filter(movie => movie.id !== action.payload)
			}

		case ADD_MOVIE:
			return{
				...state,
				movies: [...state.movies, action.payload]
			}

		case GET_MOVIE:
			return{
				...state,
				movie: action.payload
			}

		default:
			return state
	}
}