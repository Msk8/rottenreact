import { GET_REVIEWS, ADD_REVIEW  } from '../actions/types.js'

const initialState = {
	something: 'text',
	reviews: []
}

export default function(state = initialState, action) {
	switch(action.type){
		case GET_REVIEWS:
			return{
				...state,
				reviews: action.payload
			}

		case ADD_REVIEW:
			return{
				...state,
				review: [...state.reviews, action.payload]
			}

		default:
			return state
	}
}