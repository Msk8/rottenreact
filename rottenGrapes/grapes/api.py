from grapes.models import Movie, Review
from rest_framework import viewsets, permissions
from .serializers import MovieSerializer, ReviewSerializer

class MovieViewSet(viewsets.ModelViewSet):
	queryset = Movie.objects.all()
	permission_classes = [
		permissions.AllowAny
	]
	serializer_class = MovieSerializer

class ReviewViewSet(viewsets.ModelViewSet):
	#queryset = Review.objects.all()
	permission_classes = [
		permissions.AllowAny
	]
	serializer_class = ReviewSerializer

	def get_queryset(self):
		queryset = Review.objects.all()
		movie = self.request.query_params.get('movie',None)
		if movie is not None:
			queryset = queryset.filter(movie=movie)
		return queryset