# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

GENRES_CHOICES = (("Action","Action"),("Comedy","Comedy"),("Drama","Drama"), ("Horror","Horror"),("Romance","Romance"),)
CLASSIFICATIONS = (("PG-13","PG-13"),("G","G"),("R","R"),)

# Create your models here.
class Movie(models.Model):
	name = models.CharField(max_length=250)
	year_of_release= models.IntegerField(editable=False)
	#image = models.ImageField(upload_to='movie/pictures', blank=True)
	release_date = models.DateField()
	classification = models.CharField(max_length=250, choices=CLASSIFICATIONS)
	genre = models.CharField(max_length=250, choices=GENRES_CHOICES)
	duration = models.IntegerField()

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.year_of_release = self.release_date.year
		super(Movie,self).save(*args, **kwargs)

class Review(models.Model):
	movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
	star_rating = models.IntegerField(null=False, validators = [MaxValueValidator(5),MinValueValidator(0)])
	#user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
	comment = models.CharField(max_length=250)

	def __str__(self):
		return u'%s stars ' % (self.star_rating)