from rest_framework import serializers
from .models import Movie,Review

class MovieSerializer(serializers.ModelSerializer):
	image = serializers.ImageField(required=False, allow_empty_file=True)

	class Meta:
		model = Movie
		fields = '__all__'	

class ReviewSerializer(serializers.ModelSerializer):
	class Meta:
		model = Review
		fields = '__all__'