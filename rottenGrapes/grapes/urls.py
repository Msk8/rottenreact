from rest_framework import routers
from .api import MovieViewSet, ReviewViewSet

router = routers.DefaultRouter()
router.register('api/movies', MovieViewSet, 'movies')
router.register('api/reviews', ReviewViewSet, 'reviews')

urlpatterns = router.urls